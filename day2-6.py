set1 = {'A', 'P', 'P', 'L', 'E'}
print type(set1), set1
set2 = set(list('APPPLE'))
print type(set2), set2
set3 = {'B', 'A', 'N', 'A', 'N', 'A'}
print type(set3), set3
print set1 | set3
print set1 & set3
print set1 ^ set3
print (set1 & set3) | (set1 ^ set3)
set1.add('X')
print set1
set2.add(145)
print set2
#set3.add(set2)
print len(set2)
for x in set2:
    print x
