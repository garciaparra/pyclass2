a1 = 'abcdefg123456z7'
print 'cde' in a1, 'g12' in a1, 'G12' in a1
print 3 * a1, a1 * 3, a1 + a1 + a1
a2 = a1 * 5
print a2[0:10:3]
print a1[::4]
print len(a1), min(a1), max(a1)
a3 = 'abc123ABC'
print min(a3), max(a3)
print a1.index('a'), a1.index('g')
#print a1.index('A')
print a1.count('a'), a1.count('g'), a1.count('G')