print 5 + 6, 5 - 6, 5 * 6, 5 / 6, 5 ** 6
print 5 % 6, 6 % 5
print 12 ** 2
print 12 ^ 2, 12 ^ 3, 12 ^ 4
x = 5
y = 3
print x ** y, y ** x
print type(5), type(3.14), type(2 + 3j)
a = 5 + 3j
b = 4 - 7j
print a + b, a - b, a * b, a ** 2, b ** 2
print abs(a), (a.real**2+a.imag**2)**0.5
print b, b.conjugate()