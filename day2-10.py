def sample_variable_key_arguments(fix1, fix2, **kwargs):
    print "fix part:", fix1, fix2
    for k, v in kwargs.items():
        print('key=%s,value=%s' % (k, v))


sample_variable_key_arguments("hi", "hello")
sample_variable_key_arguments("hi", "hello", course='BDPY', duration=35)
newCourse = {'course': 'PYKT', 'period': 'next week'}
sample_variable_key_arguments('we can', 'xxxxx', **newCourse)
