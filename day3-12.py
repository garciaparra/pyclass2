# encoding=UTF_8
import functools
import graphviz as gv
from itertools import combinations, permutations

graph = functools.partial(gv.Graph, format='svg')
digraph = functools.partial(gv.Digraph, format='svg')


def add_nodes(graph, nodes):
    for n in nodes:
        if isinstance(n, tuple):
            graph.node(n[0], **n[1])
        else:
            graph.node(n)
    return graph


def add_edges(graph, edges):
    for e in edges:
        if isinstance(e[0], tuple):
            graph.edge(*e[0], **e[1])
        else:
            graph.edge(e[0], e[1])
            # graph.edge(*e)
    return graph


a1 = graph()
nodes = list('ABCDE')
a1 = add_nodes(a1, nodes)
edges = combinations(nodes, 2)
a1 = add_edges(a1, edges)
a1.render('graph\\lab67_a1')

a2 = digraph()
nodes2 = [('A', {'label': 'Node A'}), ('B', {'label': 'node B'}), ('C', {'label': u'中文'}), ('D', {})]
a2 = add_nodes(a2, nodes2)
edges2 = [(('A', 'B'), {'label': 'Edge1'}), (('A', 'C'), {'label': "Edge2"}), (('B', 'C'), {'label': u'某種關聯'})
    , (('B', 'D'), {})]
a2 = add_edges(a2, edges2)
a2.render('graph\\lab67_a2')
style = {
    'graph': {
        'label': u'其它型態',
        'fontsize': '24',
        'fontcolor': '#882200',
        'bgcolor': '#FF99EE',
        'rankdir': 'TB',
        'fillcolor': '#889922'
    },
    'nodes': {
        'fontname': 'Consolas',
        'shape': 'box',
        'fontcolor': 'green',
        'color': 'black',
        'style': 'filled',
        'fillcolor': '#FF9966'
    },
    'edges': {
        'style': 'dotted',
        'color': '#002288',
        'arrowhead': 'open',
        'fontname': 'Courier',
        'fontsize': '24',
        'fontcolor': '#220088'
    }
}


def apply_style(graph, styles):
    graph.graph_attr.update(('graph' in styles and styles['graph']) or {})
    graph.node_attr.update(('nodes' in styles and styles['nodes']) or {})
    graph.edge_attr.update(('edges' in styles and styles['edges']) or {})
    return graph


a3 = apply_style(a2, style)
a3.render('graph\\lab67_a3')
