class Emp:
    pass


class Engineer(Emp):
    pass


class PM(Emp):
    pass


class HR(Emp):
    pass


emp1 = Emp()
engineer1 = Engineer()
pm1 = PM()
hr1 = HR()
staffs = [(emp1, "Employee 1"), (engineer1, "Engineer 1"),
          (pm1, "Project Manager1"), (hr1, "Human Resource 1")]
classes = [Emp, PM, HR, Engineer]
for staff, name in staffs:
    for emp_class in classes:
        isA = isinstance(staff, emp_class)
        message1 = 'is a' if isA else 'is not a'
        print name, message1, emp_class.__name__

for class1 in classes:
    for class2 in classes:
        isSubclass = issubclass(class1, class2)
        message = '{0} a subclass of '.format('is' if isSubclass else 'is not')
        print class1.__name__, message, class2.__name__
