from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from day4_11 import User, Base

# engine = create_engine('sqlite:///db\\test93.sqlite', echo=True)
engine_mysql = create_engine('mysql+mysqlconnector://user1:mysql@localhost:3306/pythondemo', echo=True, pool_recycle=3600)
print([User.__tablename__, User.__table__])

# Base.metadata.create_all(engine)
#
# generic_session = sessionmaker(bind=engine)
Base.metadata.create_all(engine_mysql)

generic_session = sessionmaker(bind=engine_mysql)

session1 = generic_session()
user1 = User(name='user1', fullname='Mark User 1', password='markpassword')
user2 = User(name='user2', password='12345', fullname="James user2")
session1.add(user1)
session1.add(user2)
session1.commit()

session2 = generic_session()
all_users = session2.query(User)
for user in all_users:
    print user
session2.commit()

session3 = generic_session()
myUser = session3.query(User).filter_by(name='user1').first()
print '[1]dirty instance:', [session3.dirty]
myUser.fullname = 'MarkHo'
print '[2]dirty instance:', [session3.dirty]
staff1 = User(name='Tim',fullname='Tim Chen',password='54231')
session3.add_all([staff1])
print '[3]dirty instance:', [session3.dirty]
print '[3]new instance:', [session3.new]
session3.commit()

session4 = generic_session()
all_users = session4.query(User)
for user in all_users:
    print user

session5 = generic_session()
userToBeDeleted = session5.query(User).filter_by(name='user2').first()
session5.delete(userToBeDeleted)
session5.commit()

session4 = generic_session()
all_users = session4.query(User)
for user in all_users:
    print user