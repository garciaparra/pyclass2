x = 10
y = 3.14159
z = '3.14159'
k = True


class Foo():
    def __init__(self):
        self.x = 10
        self.y = 3.14159
        self.z = '3.14159'
        self.k = True


foo1 = Foo()


def changeVariable():
    global x
    global y
    global z
    global k
    x += 1
    y *= 2
    z *= 2
    k ^= k
    foo1.x += 1
    foo1.y *= 2
    foo1.z *= 2
    foo1.k ^= foo1.k

print('before change, x=%d,y=%.4f,z=%s, k=%d' % (x, y, z, k))
print('before change, foo.x=%d,foo.y=%.4f,foo.z=%s, foo.k=%d' % (foo1.x, foo1.y, foo1.z, foo1.k))
changeVariable()
print('after change, x=%d,y=%.4f,z=%s,k=%d' % (x, y, z, k))
print('before change, foo.x=%d,foo.y=%.4f,foo.z=%s, foo.k=%d' % (foo1.x, foo1.y, foo1.z, foo1.k))
changeVariable()
print('after change, x=%d,y=%.4f,z=%s,k=%d' % (x, y, z, k))
print('before change, foo.x=%d,foo.y=%.4f,foo.z=%s, foo.k=%d' % (foo1.x, foo1.y, foo1.z, foo1.k))
