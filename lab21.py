a1 = list('abcde')
a2 = a1
a3 = a1[:]
a4 = list(a1)
print a1, a2, a3, a4
print 'address is %s, %s,%s,%s' % (hex(id(a1)), hex(id(a2)),
                                   hex(id(a3)), hex(id(a4)))
a1[0] = '*'
a2[1] = 'o'
a1[2] = 'O'
a2[3] = '0'
a1[4] = '$'

a3[0] = 'Q'
a4[1] = 'P'

print 'address is %s, %s,%s,%s' % (hex(id(a1)), hex(id(a2)),
                                   hex(id(a3)), hex(id(a4)))
print a1, a2, a3, a4
