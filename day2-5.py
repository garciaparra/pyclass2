a1 = ('hello')
a2 = ('hello',)
a3 = ()
a4 = ('a', 'b')
print type(a1), type(a2), type(a3), type(a4)
print a1 * 2
print a2 * 2
dayOfWeek = ('Sunday', 'Monday')
dayOfWeek += ('Tuesday',)
print dayOfWeek
print dayOfWeek[0], dayOfWeek[2], dayOfWeek[1]
print 'Sunday' in dayOfWeek
print 'Xunday' in dayOfWeek


def split_head(sequence):
    return sequence[0], sequence[1:]


t1 = (3, 4, 5, 6, 7, 8, 9)
l1 = [4, 5, 6, 7, 8, 9, 10]
x1, x2 = split_head(t1)
y1, y2 = split_head(l1)
print type(x1), x1
print type(x2), x2
print type(y1), y1
print type(y2), y2
