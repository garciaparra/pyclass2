#encoding=UTF-8
import requests

base_url = 'https://python-9b26a.firebaseio.com/%s.json'

url1 = base_url % 'string'
data1 = 'hello firebase from pycharm'
result1 = requests.put(url1, json=data1)
print result1.status_code, result1.content
url2 = base_url % 'int'
data2 = 255
result2 = requests.put(url2, json=data2)
print result2.status_code, result2.content
url3 = base_url % 'list'
data3 = [3, 5, 8, 200]
result3 = requests.put(url3, json=data3)
print result3.status_code, result3.content
url4 = base_url % 'dict'
data4 = {'course': 'bdpy', 'duration': 35}
result4 = requests.put(url4, json=data4)
print result4.status_code, result4.content
url5 = base_url % 'cstring'
data5 = '中文字串'
result5 = requests.put(url5, json=data5)
print result1.status_code, result1.content

