class MyClass:
    pass


m1 = MyClass()

print "My class type is:", type(MyClass)
print "m1 type is", type(m1)
print "m1 class is", m1.__class__
print "m1 class bases is", m1.__class__.__bases__
print 'X type equal to MyClass?', type(m1) == MyClass
