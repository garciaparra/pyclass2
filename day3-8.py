import tkFont
import Tkinter
from Tkinter import Radiobutton, Label, IntVar, Scale

def display(ev):
    label1.config(text=entry1.get())

top = Tkinter.Tk(screenName='BDPY')
value = IntVar()
myFont1 = tkFont.Font(family='Source Code Pro', size=24)

label1 = Tkinter.Label(top, text='input text',font=myFont1)
label1.pack()
entry1 = Tkinter.Entry(top, font=myFont1)
entry1.pack()
button1 = Tkinter.Button(top, font=myFont1, text="click me")
button1.pack()
entry1.bind('<Return>', display)
button1.bind('<Button-1>', display)

top.mainloop()