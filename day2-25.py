#encoding=UTF-8
import requests

base_url = 'https://python-9b26a.firebaseio.com/%s.json'

url4 = base_url % 'dict'
data4 = {'duration': 32, 'reason':'weather/typhoon'}
result4 = requests.patch(url4, json=data4)
print result4.status_code, result4.content

url3 = base_url % 'list'
data3 = {'0':300, '4':'newly added'}
result3 = requests.patch(url3, json=data3)
print result3.status_code, result3.content
