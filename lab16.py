class Foo():
    def __str__(self):
        return 'foo: string format'

    def __repr__(self):
        return 'foo: representation format'


f1 = Foo()
print f1
print (f1,)
# inside >>>
# f1 ==> repr format
# print f1 ==> str format
print 'string==>%s, repr==>%r\n' % (f1, f1)
print 'string==>{0!s}, repr==>{0!r}\n'.format(f1)