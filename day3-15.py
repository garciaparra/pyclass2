class Car:
    vendor = 'lexus'
    valid = True

c1 = Car()
c2 = Car()
print Car.vendor, c1.vendor, c2.vendor
c1.valid = False
print c1.valid, c2.valid, Car.valid
c1.color='RED'
print c1.color
c2.capacity=7
print c2.capacity
Car.maximum = 10000
print Car.maximum, c1.maximum, c2.maximum