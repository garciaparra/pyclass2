a1 = 'hello'
print type(a1)
print type((a1))
print type((a1,))
a2 = 'world'
print a1, a2
temp = a1
a1 = a2
a2 = temp
print a1, a2
a3 = 5
a4 = 7
print a3, a4
a3, a4 = a4, a3
print a3, a4
a5 = 'ken'
a6 = 3.14
print a5, a6
a5, a6 = a6, a5
print a5, a6
a7 = 'oragne'
a8 = 'apple'
a9 = 'citron'
print a7, a8, a9
a7, a8, a9 = a9, a7, a8
print a7, a8, a9