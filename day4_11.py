# pip install sqlalchemy
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    fullname = Column(String(200))
    password = Column(String(256))

    def __repr__(self):
        return "<User(name+'%s',fullname='%s',password='%s')>" % (self.name, self.fullname, self.password)

    def __str__(self):
        return repr(self)
