import Tkinter
import tkFont

message1 = 'the button clicked %d times'

counter = 0


def func1():
    global counter
    label1.config(text=message1 % counter)
    counter = counter + 1


top = Tkinter.Tk(screenName='BDPY')
myFont1 = tkFont.Font(family='Source Code Pro', size=24)
myFont2 = tkFont.Font(family='Courier New CE', size=24)

button1 = Tkinter.Button(top, text="Click Me!", font=myFont1, command=func1)
label1 = Tkinter.Label(top, text="Display something!", font=myFont2, padx=10, pady=10)
button1.pack()
label1.pack()
top.mainloop()
