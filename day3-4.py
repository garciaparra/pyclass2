import Tkinter
import tkFont
from Tkinter import Label, Button


def funcEnter(ev):
    displayLabel.config(text=displayLabel.cget('text') + '\n' + "cursor in (%d,%d)" % (ev.x, ev.y),bg='#FF0')


def funcLeave(ev):
    displayLabel.config(text=displayLabel.cget('text') + '\n' + "cursor out(%d,%d)" % (ev.x, ev.y), bg='#FF9')

def single(ev):
    displayLabel.config(text='single clicked')

def double(ev):
    displayLabel.config(text='double clicked')
def move(ev):
    displayLabel.config(text='moving...')

top = Tkinter.Tk(screenName='BDPY')
myFont1 = tkFont.Font(family='Source Code Pro', size=24)
displayLabel = Label(top, text="display status", font=myFont1, bg='#FF9')
displayLabel2 = Label(top, text='check this area', font=myFont1, bg='#F9F', padx=100, pady=200)
button1 = Button(top, text='click me', font=myFont1, bg='#9FF')
displayLabel.pack()
displayLabel2.pack()
displayLabel2.bind('<Enter>', funcEnter)
displayLabel2.bind('<Leave>', funcLeave)
button1.bind('<Button-1>',single)
button1.bind('<Double-3>',double)
button1.bind('<B2-Motion>',move)
button1.pack()
top.mainloop()
