# encoding=UTF-8
str1 = '{:<10}==='
print str1.format('中文')
str2 = '{:_<10}==='
print str2.format('中文')
str3 = '{:*>10}==='
print str3.format('中文')
str4 = '{:\'^10}==='
print str4.format('中文')
str5 = '%.9s'
print str5 % ('一個非常長的中文字串')
str6 = '%.9s'
print str6 % (u'一個非常長的中文字串')
str7 = '%.*s'
print str7 % (3, '一個非常長的中文字串')
str8 = '{:.{}}'
print str8.format('一個非常長的中文字串', 6)