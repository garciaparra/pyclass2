#encoding=UTF-8
import requests

base_url = 'https://python-9b26a.firebaseio.com/%s.json'
url1 = base_url % 'string'
url2 = base_url % 'int'
url3 = base_url % 'list'
url4 = base_url % 'dict'
r1 = requests.get(url1)
print r1.status_code, type(r1.json()), r1.json()
r2 = requests.get(url2)
print r2.status_code, type(r2.json()), r2.json()
r3 = requests.get(url3)
print r3.status_code, type(r3.json()), r3.json()
r4 = requests.get(url4)
print r4.status_code, type(r4.json()), r4.json()