import itertools

a1 = list('abcde')
a2 = list('XYZ')
a3 = [5, 7, 9]
p = itertools.chain(a1, a3, a2)
print type(p), p
# print[e for e in p]
# print[e for e in p] # second time will be empty
result1 = [e for e in p]
print result1
print result1
p2 = itertools.chain(a1, a3, a2)
result2 = list(p2)
print result2, result2
q = itertools.permutations(a1, 2)
result2 = list(q)
print len(result2), result2
r = itertools.combinations(a1, 2)
result3 = list(r)
print len(result3), result3
