# pip install folium
import folium
import pandas as pd

sample_data = pd.read_csv('.\\data\\data4.csv', sep=',')
print sample_data.shape
print sample_data.columns
sample_data.columns = ['section', 'road', 'road_detail', 'lon', 'lat', 'extra']
print sample_data.head()

taipei = [25.103051, 121.522477]
zoom = 14
map_osm = folium.Map(location=taipei, zoom_start=zoom)
# plot Icon by GPS position
for i in range(len(sample_data)):
    coordinate = [sample_data.iloc[i, 4], sample_data.iloc[i, 3]]
    addressDetail = '%s,%s' % (sample_data.iloc[i, 1], sample_data.iloc[i, 2])
    addressDetail = unicode(addressDetail, 'utf-8')
    message = '[%d](%s)%s' % (i, coordinate,addressDetail)
    print message
    icon1 = folium.Icon(color='green', icon='info-sign')
    folium.Marker(coordinate, icon=icon1, popup=message).add_to(map_osm)
    # print('the %dth position, value=%s'%(i, coordinate))
map_osm.save('.\\map\\output1.html')
