sales = {'iphone8': 500, 'iphone8+': 600, 'iphoneXS': 700, 'iphoneXS+': 800}
print type(sales), len(sales)
print sales['iphoneXS+'], sales['iphoneXS']
# print sales['htc']
print sales.get('iphoneXS'), sales.get('htc')
for i in sales:
    print 'direct iterate a dict, value=', i
for i in sales.keys():
    print 'iterate a dict key, value=', i
total = 0
for i in sales.values():
    print 'iterate a dict value, value=', i
    total = total + i
print 'total sales amount=%d' % total
for xx in sales.items():
    print type(xx), xx[0], xx[1]
for k, v in sales.items():
    print '[(%s)-->%s]' % (str(k), str(v))

sales.update({'iphone8+': 300, 'ipad pro': 150})
print sales
del sales['iphone8']
print sales
print 'iphone8+' in sales, 'asus' in sales