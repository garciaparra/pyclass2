class Member:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return  self.name

    def __repr__(self):
        return  str(self)

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)


m1 = Member('Mark')
m2 = Member('John')
s1 = {m1, m2}
print "only mark & john, set=", s1
m3 = Member('Mark')
s1.add(m3)
print "add a mark again, set=", s1
