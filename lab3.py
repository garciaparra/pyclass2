def getdigit(x):
    """

    :type x: int or float
    """
    returndigit = 0
    while x > 0:
        returndigit += 1
        x = x / 10
    return returndigit


print getdigit(2 ** 600),2**600
