# encoding=utf-8
a1 = ''  # comment
# this is also comment
a2 = u"hello"
a3 = '''welcome'''
a4 = u"""Hello World"""
a5 = '中文'
a6 = u'中文'
print type(a1), a1
print type(a2), a2
print type(a3), a3
print type(a4), a4
print a5, type(a5), len(a5)
print a6, type(a6), len(a6)