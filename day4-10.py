import sqlite3
import time

connection1 = sqlite3.connect('.\\db\\test89.sqlite')
emp1 = {'NAME': 'Mark', 'AGE': 43, 'DEPT': 1, 'ADDR': 'Taipei'}
emp2 = {'NAME': 'John', 'AGE': 38, 'DEPT': 1, 'ADDR': 'Hsinchu'}
emp3 = {'NAME': 'Tim', 'AGE': 27, 'DEPT': 3, 'ADDR': 'Taipei'}
emp4 = {'NAME': 'Ken', 'AGE': 49, 'DEPT': 1, 'ADDR': 'Taipei'}
emp5 = {'NAME': 'Sherry', 'AGE': 22, 'DEPT': 3, 'ADDR': 'Hsinchu'}

employees = [emp1, emp2, emp3, emp4, emp5]
sql_dml = "INSERT INTO EMPLOYEE (NAME, AGE, DEPT, ADDRESS) VALUES(?,?,?,?)"
start_time = time.time()
for i in range(1000):
    for emp in employees:
        connection1.execute(sql_dml, (emp['NAME'], emp['AGE'], emp['DEPT'], emp['ADDR']))
connection1.commit()

print "total=%s seconds" % (time.time() - start_time)
connection1.close()
