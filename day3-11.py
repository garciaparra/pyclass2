import functools
import graphviz as gv

graph = functools.partial(gv.Graph, format='svg')
digraph = functools.partial(gv.Graph, format='svg')

def add_nodes(graph, nodes):
    for n in nodes:
        if isinstance(n, tuple):
            pass
        else:
            graph.node(n)
    return graph

a1 = graph()
a1 = add_nodes(a1, list('ABCDE'))
a1.render('graph\\lab67_a1')

