file1 = open('.\\data\\Python_Introduction')
readme_txt1 = file1.read()
file1.close()

print type(readme_txt1), len(readme_txt1), readme_txt1[:50]

with open('.\\data\\Python_Introduction') as file2:
    readme_txt2 = file2.read().decode('utf8')

print type(readme_txt2), len(readme_txt2), readme_txt2[:50]

file3 = open('.\\data\\clone', 'w')
file3.write(readme_txt1)
file3.close()

with open('.\\data\\clone2','w') as file4:
    file4.write(readme_txt2.encode('utf8'))
