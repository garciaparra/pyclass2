class Emp():
    gradeLevel = 6

    def startWork(self):
        pass

    def endWork(self):
        pass


class RD(Emp):
    def __init__(self):
        self.currentGrade = self.gradeLevel

    def startWork(self):
        print self.currentGrade, 'start to R&D'

    def endWork(self):
        print self.currentGrade, 'end work'


rd1 = RD()
rd1.currentGrade = 8
print rd1.currentGrade
rd1.startWork()
rd1.endWork()


class PM(Emp):
    pass


print Emp.gradeLevel, RD.gradeLevel, PM.gradeLevel

PM.gradeLevel = 8
print Emp.gradeLevel, RD.gradeLevel, PM.gradeLevel
Emp.gradeLevel = 7
print Emp.gradeLevel, RD.gradeLevel, PM.gradeLevel
