import Tkinter
import tkFont
from Tkinter import Radiobutton, Label, IntVar

str1 = 'you choose google'
str2 = 'you choose apple'


def func1():
    label1.config(text=str1)


def func2():
    label1.config(text=str2)


def funcX():
    value = var1.get()
    if value == 1:
        label1.config(text=str1)
    elif value == 2:
        label1.config(text=str2)


top = Tkinter.Tk(screenName='BDPY')
myFont1 = tkFont.Font(family='Source Code Pro', size=24)
label1 = Label(top, text='which one will you choose', font=myFont1)
var1 = IntVar(0)
button1 = Radiobutton(top, text='google pixel3', font=myFont1, variable=var1, value=1, command=funcX)
button2 = Radiobutton(top, text='apple xs max', font=myFont1, variable=var1, value=2, command=funcX)
label1.pack()
button1.pack()
button2.pack()
top.mainloop()
