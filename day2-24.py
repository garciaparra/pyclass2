import requests

base_url = 'https://python-9b26a.firebaseio.com/%s.json'
url6 = base_url % 'biddings'
bidding = {'name': 'mark', 'price': 100}
r6 = requests.post(url6, json=bidding)
print r6.status_code, r6.content

for i in range(0, 10):
    url6 = base_url % 'biddings'
    bidding = {'name': 'mark', 'price': 100 * i + 100}
    r6 = requests.post(url6, json=bidding)
    print r6.status_code, r6.content
