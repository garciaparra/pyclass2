# encoding=UTF-8
import Tkinter
import tkFont

top = Tkinter.Tk()
for x in tkFont.families():
    print x
myFont1 = tkFont.Font(family='標楷體', size=24)
myFont2 = tkFont.Font(family='Courier New CE', size=24)
label1 = Tkinter.Label(top, text='my first 標籤', font=myFont1,fg='#F00',bg='#0FF', padx='10')
label2 = Tkinter.Label(top, text='my second label', font=myFont2, fg='#0F0', bg='#FF0', pady='30')
label2.pack()
label1.pack()
top.mainloop()

