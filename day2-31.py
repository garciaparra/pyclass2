from Tkinter import Tk, Frame, Entry


class MyWindow(Frame):
    def __init__(self, parent=None):
        Frame.__init__(self, parent)
        self.parent = parent
        self.pack()
        self.make_widgets()

    def make_widgets(self):
        self.winfo_toplevel().title('BDPY')


root = Tk()
window = MyWindow(root)
root.minsize(1024, 768)
root.maxsize(1024, 768)
root.mainloop()
