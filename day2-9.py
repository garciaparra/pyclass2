def sample_variable_arguments(fix1, fix2, *args):
    print "fix part:", fix1, fix2
    for i in args:
        print "variable part has a parameter:", i


sample_variable_arguments("hi", 'Hello')
sample_variable_arguments(1, 2, 3, 4, 5)
sample_variable_arguments('1', '2', [1, 2], (1, 2))
list1 = [1, 3, 5, 7, 9]
sample_variable_arguments("put a ", "list", *list1)
t1 = (2, 4, 6, 8, 10)
sample_variable_arguments("with a ", "tuple:", *t1)
s1 = {'A', 'P', 'P', 'L', 'E'}
sample_variable_arguments("with a,", "set:", *s1)
l2 = list('APPLE')
sample_variable_arguments("with a list", "contain several chars:", *l2)
obj1 = {'name': 'BDPY', 'duration': 35}
sample_variable_arguments("with a object", "chars:", *obj1)
