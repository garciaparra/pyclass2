r1 = range(0, 20)
print type(r1), r1
print r1, r1, r1
r2 = range(0, 20, 2)
print r2
r3 = range(0, 20, 3)
print r3
# in c:\python27\Scripts
# pip install numpy
import numpy as np

r4 = np.arange(0, 20)
print type(r4), r4
r5 = np.arange(0, 2, 0.1)
print type(r5), r5
r6 = np.linspace(0,2, 10)
print type(r6), r6