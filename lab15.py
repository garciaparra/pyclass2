a = 5
b = a * a * 3.14159
str1 = 'radius=%f, area=%f\n'
print str1 % (a, b)
str2 = 'radius=%.2f, area=%.4f\n'
print str2 % (a, b)
str3 = 'radius=%(radius).2f, area=%(area).4f\n'
print str3 % {'radius': a, 'area': b}
print str3 % {'area': b, 'radius': a}
str4 = 'radius={}, area={}'
print str4.format(a, b)
str5 = 'radius={:.2f}, area={:.2f}'
print str5.format(a, b)
str6 = 'radius={0:.2f}, area={1:.2f}'
print str6.format(a, b)
str7 = 'radius={1:.2f}, area={0:.2f}'
print str7.format(b, a)
str8 = 'radius={radius:.2f}, area={area:.2f}'
print str8.format(radius=a, area=b)