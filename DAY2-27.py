# encoding=UTF-8
import requests

base_url = 'https://python-9b26a.firebaseio.com/%s.json'
url1 = base_url % 'string'
url2 = base_url % 'int'
url3 = base_url % 'list'
url4 = base_url % 'dict'

r1 = requests.delete(url1)
print r1.status_code, r1.content
r2 = requests.delete(url2)
r3 = requests.delete(url3)
r4 = requests.delete(url4)
url5 = base_url % ""
r5 = requests.delete(url5)

