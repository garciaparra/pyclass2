# pip install requests
# pip install bs4
# pip install pillow
import requests

url1 = 'https://bugzilla.mozilla.org/rest/bug/35'

r1 = requests.get(url1)
print r1.status_code, r1.headers['content-type']
print r1.content, r1.json()
bugs = r1.json()
print type(bugs), type(bugs['bugs'])
firstBug = bugs['bugs'][0]
for k, v in firstBug.items():
    print 'key=%s, value=%s' % (str(k), str(v))
details = firstBug['cc_detail']
for detail in details:
    print detail
