import tkFont
import Tkinter
from Tkinter import Radiobutton, Label, IntVar, Scale


def func(scale):
    #label1.config(text=formattedText % value.get())
    label1.config(text=formattedText % int(scale))


formattedText = "value=%d"
top = Tkinter.Tk(screenName='BDPY')
value = IntVar()
myFont1 = tkFont.Font(family='Source Code Pro', size=24)
label1 = Tkinter.Label(top, text=formattedText % value.get(), font=myFont1)

scale1 = Tkinter.Scale(top, label='Scale', font=myFont1, from_=0, to=100, showvalue=False, variable=value, orient='h', command=func)
label1.pack()
scale1.pack()
top.minsize(250, 250)
top.maxsize(250, 250)
top.mainloop()
