import sqlite3
connection1 = sqlite3.connect('.\\db\\test89.sqlite')
print 'db create successfully'
drop_and_create_sql='''
DROP TABLE IF EXISTS EMPLOYEE;
CREATE TABLE EMPLOYEE
(ID INTEGER PRIMARY KEY AUTOINCREMENT,
NAME TEXT NOT NULL,
AGE INT NOT NULL,
DEPT INT, 
ADDRESS CHAR(50));
'''
connection1.executescript(drop_and_create_sql);

connection1.close()