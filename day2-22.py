from urllib2 import urlopen
from PIL import Image

img1 = 'https://cache.movidius.com/images/remote/http_cache.movidius.com/images/made/images/remote/https_movidius-uploads.s3.amazonaws.com/1522334117-movidius-chip-angle-blue-bg-reflection_640_445_s_c1.png'

fileToSave = urlopen(img1)
image = Image.open(fileToSave)
image.save('.\\images\\normal.png')
half = (image.size[0] // 2, image.size[1] // 2)
half_image = image.resize(half)
half_image.save('.\\images\\half.png')

r90 = image.transpose(Image.ROTATE_90)
r90.save('.\\images\\r90.png')

anyR = image.rotate(45)
anyR.save('.\\images\\r45.png')
