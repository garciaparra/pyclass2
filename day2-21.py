import requests
import bs4

url1 = 'https://www.mobile01.com/'
r1 = requests.get(url1)
soup = bs4.BeautifulSoup(r1.content, 'html.parser')

courses = soup.find('div', {'id': 'hot-posts'})
links = courses.find_all('a')
for link in links:
    print link

base_url = 'https://www.mobile01.com/category.php?id=%d'
boards = [2, 4, 5]

for id in boards:
    url = base_url % id
    result = requests.get(url)
    soup = bs4.BeautifulSoup(result.content, 'html.parser')
    print soup.title
    courses = soup.find('div', {'id': 'hot-posts'})
    links = courses.find_all('a')
    for link in links:
        print link
