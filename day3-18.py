class Rectangle:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def calculate_area(self):
        return self.width * self.height


rect1 = Rectangle(3, 5)
rect2 = Rectangle(7, 9)
print rect1.width, rect1.height, rect1.calculate_area()
print rect2.width, rect2.height, rect2.calculate_area()
