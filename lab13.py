a1 = ['a', 'b', 'c', 'd', 'e']
print type(a1), a1, len(a1)
a2 = list('abcde')
print type(a2), a2, len(a2)
a3 = 'abcde'
print type(a3), a3, len(a3)
a2[0] = 'A'
a2[1] = 'B'
a2[4] = 'E'
print a2
# a3[0] = 'A'
a2[2:4] = '^'
print a2
a4 = list('abcde12345' * 6)
a4[0:20:3] = '*' * len(a4[0:20:3])
print a4