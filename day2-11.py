def fix_part_parameter(par1, par2, par3):
    print "fix part=", par1, par2, par3


fix_part_parameter(3, 4, 5)

k1 = list('ABC')
print(type(k1))
fix_part_parameter(*k1)
t1 = ('apple', 'banana', 'citron')
fix_part_parameter(*t1)
newCourse = {'course': 'PYKT', 'period': 'next week', 'duration': 35}
fix_part_parameter(*newCourse)
