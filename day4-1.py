import matplotlib.pyplot as plt
import numpy as np

t = np.arange(0, 10, 0.5)
plt.figure(1)

plt.subplot(311)
plt.plot(t, t, 'r-')

plt.subplot(3, 1, 2)
plt.plot(t, t ** 2, 'g*')

plt.subplot(313)
plt.plot(t, t ** 3, 'b^')

plt.show()
