import Tkinter
import tkFont


def motion(event):
    message1.config(text= '[%d,%d]' % (event.x, event.y))


top = Tkinter.Tk(screenName='BDPY')
myFont1 = tkFont.Font(family='Source Code Pro', size=24)
message1 = Tkinter.Message(top, text='move to', font=myFont1)
label1 = Tkinter.Label(top, text='move area', font=myFont1, padx=200, pady=200, bg='#99F')
message1.pack()
label1.pack()
label1.bind('<Motion>', motion)
top.mainloop()
